package ot.polyackov.microservices.architecture.course.delivery.core.domain.courier.aggregate;

import static java.lang.String.format;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static ot.polyackov.microservices.architecture.course.delivery.core.domain.util.JsonParseUtil.getValidObject;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import ot.polyackov.microservices.architecture.course.delivery.core.domain.order.aggregate.Order;

class CourierTest {

    @Nested
    class CreateTest {

        @Test
        void create() {
            var name = "Пушкин Александр Сергеевич";
            var transport = Transport.CAR;


            var actual = Courier.create(name, transport);


            var expected = getValidObject("files/core/domain/courier.aggregate/Courier_NOT_AVAILABLE.json", Courier.class);
            assertAll(
                () -> assertNotNull(actual.getId()),
                () -> assertEquals(expected.getName(), actual.getName()),
                () -> assertEquals(expected.getStatus(), actual.getStatus()),
                () -> assertEquals(expected.getTransport(), actual.getTransport()),
                () -> assertEquals(expected.getCurrentLocation(), actual.getCurrentLocation())
            );
        }

        @Test
        void create_whenNameIsBlank_thenThrow() {
            var name = "    ";
            var transport = Transport.CAR;


            var actual = assertThrows(IllegalArgumentException.class, () -> Courier.create(name, transport));


            assertEquals("Courier.name should be Not Blank.", actual.getMessage());
        }

    }

    @Nested
    class StartOrderTest {

        @Test
        void startWorkingDay_whenPreviousStatusIsNotAvailable_thenSet() {
            var actual = getValidObject("files/core/domain/courier.aggregate/Courier_NOT_AVAILABLE.json", Courier.class);
            actual.startWorkingDay();


            var expected = getValidObject("files/core/domain/courier.aggregate/Courier_READY.json", Courier.class);
            assertAll(
                () -> assertEquals(expected.getStatus(), actual.getStatus())
            );
        }

        @Test
        void startWorkingDay_whenPreviousStatusIsNotNotAvailable_thenTrow() {
            var courier = getValidObject("files/core/domain/courier.aggregate/Courier_READY.json", Courier.class);


            var actual = assertThrows(IllegalArgumentException.class, courier::startWorkingDay);


            assertEquals(format("Courier should be in status %s.", CourierStatus.NOT_AVAILABLE), actual.getMessage());
        }

    }

    @Nested
    class FinishOrderTest {

        @Test
        void finishWorkingDay_whenPreviousStatusIsReady_thenSet() {
            var actual = getValidObject("files/core/domain/courier.aggregate/Courier_READY.json", Courier.class);
            actual.finishWorkingDay();


            var expected = getValidObject("files/core/domain/courier.aggregate/Courier_NOT_AVAILABLE.json", Courier.class);
            assertAll(
                () -> assertEquals(expected.getStatus(), actual.getStatus())
            );
        }


        @Test
        void finishWorkingDay_whenPreviousStatusIsNotReady_thenThrow() {
            var courier = getValidObject("files/core/domain/courier.aggregate/Courier_BUSY.json", Courier.class);


            var actual = assertThrows(IllegalArgumentException.class, courier::finishWorkingDay);


            assertEquals(format("Courier should be in status %s.", CourierStatus.READY), actual.getMessage());
        }

    }

    @Nested
    class AssignOrderTest {


        @Test
        void assignOrder_whenPreviousStatusIsReady_thenAssign() {
            var order = getValidObject("files/core/domain/order.aggregate/Order_CREATED.json", Order.class);


            var actual = getValidObject("files/core/domain/courier.aggregate/Courier_READY.json", Courier.class);
            actual.assignOrder(order);


            var expected = getValidObject("files/core/domain/courier.aggregate/Courier_BUSY.json", Courier.class);
            assertEquals(expected.getStatus(), actual.getStatus());
        }


        @Test
        void assignOrder_whenPreviousStatusIsNotReady_thenThrow() {
            var order = getValidObject("files/core/domain/order.aggregate/Order_CREATED.json", Order.class);
            var courier = getValidObject("files/core/domain/courier.aggregate/Courier_BUSY.json", Courier.class);


            var actual = assertThrows(IllegalArgumentException.class, () -> courier.assignOrder(order));


            assertEquals(format("Courier should be in status %s.", CourierStatus.READY), actual.getMessage());
        }

    }

    @Nested
    class TimeToDeliveryLocationTest {

        @Test
        void getMoveCountToDeliveryLocation_whenIsEnoughOneMove_thenReturnOne() {
            var order = getValidObject("files/core/domain/order.aggregate/Order_ASSIGNED.json", Order.class);
            var courier = getValidObject("files/core/domain/courier.aggregate/Courier_BUSY.json", Courier.class);


            var actual = courier.getMoveCountToDeliveryLocation(order);


            assertEquals(1, actual);
        }

        @Test
        void getMoveCountToDeliveryLocation_whenCourierHasAlreadyArrivedAtDeliveryLocation_thenReturnZero() {
            var order = getValidObject("files/core/domain/order.aggregate/Order_ASSIGNED.json", Order.class);
            var courier = getValidObject("files/core/domain/courier.aggregate/Courier_BUSY_hasAlreadyArrived.json", Courier.class);


            var actual = courier.getMoveCountToDeliveryLocation(order);


            assertEquals(0, actual);
        }

    }

    @Nested
    class MoveToLocationTest {

        @Test
        void moveToLocation_whenIsEnoughOneMove_thenCourierLocationEqualsOrderLocation() {
            var order = getValidObject("files/core/domain/order.aggregate/Order_ASSIGNED.json", Order.class);
            var courier = getValidObject("files/core/domain/courier.aggregate/Courier_BUSY.json", Courier.class);


            courier.moveToLocation(order);


            var expected = getValidObject("files/core/domain/courier.aggregate/Courier_BUSY_hasAlreadyArrived.json", Courier.class);
            assertEquals(expected.getCurrentLocation(), courier.getCurrentLocation());
        }

        @Test
        void moveToLocation_whenPreviousStatusIsNotBusy_thenThrow() {
            var courier = getValidObject("files/core/domain/courier.aggregate/Courier_READY.json", Courier.class);
            var order = getValidObject("files/core/domain/order.aggregate/Order_ASSIGNED.json", Order.class);


            var actual = assertThrows(IllegalArgumentException.class, () -> courier.moveToLocation(order));


            assertEquals(format("Courier should be in status %s.", CourierStatus.BUSY), actual.getMessage());
        }

        @Test
        void moveToLocation_whenOrderWasAssignedToAnotherCourier_thenThrow() {
            var courier = getValidObject("files/core/domain/courier.aggregate/Courier_BUSY.json", Courier.class);
            var order = getValidObject("files/core/domain/order.aggregate/Order_withAnotherCourier_ASSIGNED.json", Order.class);


            var actual = assertThrows(IllegalArgumentException.class, () -> courier.moveToLocation(order));


            assertEquals("Order was assigned to another courier.", actual.getMessage());
        }
    }
}
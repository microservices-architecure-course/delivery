package ot.polyackov.microservices.architecture.course.delivery.core.domain.order.aggregate;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static ot.polyackov.microservices.architecture.course.delivery.core.domain.util.JsonParseUtil.getValidObject;

import java.util.UUID;
import org.junit.jupiter.api.Test;
import ot.polyackov.microservices.architecture.course.delivery.core.domain.shared.kernel.Location;
import ot.polyackov.microservices.architecture.course.delivery.core.domain.shared.kernel.Weight;

class OrderTest {

    @Test
    void create() {
        var id = UUID.fromString("00000000-0000-0000-0000-000000000000");
        var deliveryLocation = Location.create(3, 3);
        var weight = Weight.create(1);


        var actual = Order.create(id, deliveryLocation, weight);


        var expected = getValidObject("files/core/domain/order.aggregate/Order_CREATED.json", Order.class);
        assertAll(
            () -> assertEquals(expected.getId(), actual.getId()),
            () -> assertEquals(expected.getDeliveryLocation(), actual.getDeliveryLocation()),
            () -> assertEquals(expected.getWeight(), actual.getWeight()),
            () -> assertEquals(expected.getStatus(), actual.getStatus())
        );
    }


    @Test
    void assignCourier() {
        var courierId = UUID.fromString("cccccccc-cccc-cccc-cccc-000000000000");


        var actual = getValidObject("files/core/domain/order.aggregate/Order_CREATED.json", Order.class);
        actual.assignCourier(courierId);


        var expected = getValidObject("files/core/domain/order.aggregate/Order_ASSIGNED.json", Order.class);
        assertAll(
            () -> assertEquals(expected.getCourierId(), actual.getCourierId()),
            () -> assertEquals(expected.getStatus(), actual.getStatus())
        );
    }

    @Test
    void completed_whenOrderInStatusAssigned_thenCompleted() {
        var actual = getValidObject("files/core/domain/order.aggregate/Order_ASSIGNED.json", Order.class);
        actual.completed();


        var expected = getValidObject("files/core/domain/order.aggregate/Order_COMPLETED.json", Order.class);
        assertEquals(expected.getStatus(), actual.getStatus());
    }


    @Test
    void completed_whenOrderInStatusCreated_thenThrow() {
        var order = getValidObject("files/core/domain/order.aggregate/Order_CREATED.json", Order.class);
        var actual = assertThrows(IllegalStateException.class, order::completed);


        assertEquals("Only the assigned order can be " + OrderStatus.COMPLETED, actual.getMessage());
    }
}
package ot.polyackov.microservices.architecture.course.delivery.core.domain.shared.kernel;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

class WeightTest {

    @Test
    void create_whenWightIsCorrect_thenCreate() {
        var actual = Weight.create(10);

        assertEquals(10, actual.value());
    }


    @Test
    void create_whenWightIsNotCorrect_thenThrow() {
        var actual = assertThrows(IllegalArgumentException.class, () -> Weight.create(-10));


        assertEquals("Expected weight should be positive. Actual weight is '-10'.", actual.getMessage());
    }
}
package ot.polyackov.microservices.architecture.course.delivery.ui.adapters.http;

import static java.lang.String.format;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static ot.polyackov.microservices.architecture.course.delivery.core.domain.util.JsonParseUtil.getJson;
import static ot.polyackov.microservices.architecture.course.delivery.core.domain.util.JsonParseUtil.getValidObject;

import java.util.NoSuchElementException;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import ot.polyackov.microservices.architecture.course.delivery.core.application.command.assign.order.AssignOrderHandler;
import ot.polyackov.microservices.architecture.course.delivery.core.application.query.get.order.info.GetOrderInfoHandler;
import ot.polyackov.microservices.architecture.course.delivery.core.application.query.get.order.info.GetOrderInfoResponse;
import ot.polyackov.microservices.architecture.course.delivery.core.domain.courier.aggregate.CourierStatus;
import ot.polyackov.microservices.architecture.course.delivery.ui.adapters.http.mapper.GetOrderInfoMapperImpl;

@WebMvcTest(value = {
    OrdersController.class,

    GetOrderInfoMapperImpl.class,
})
@AutoConfigureMockMvc(addFilters = false)
class OrdersControllerTest {

    @Autowired
    private MockMvc mockMvc;


    @MockBean
    private GetOrderInfoHandler getOrderInfoHandler;
    @MockBean
    private AssignOrderHandler assignOrderHandler;


    private final String orderId = "00000000-0000-0000-0000-000000000000";


    @Nested
    class GetOrderTest {

        @Test
        void getOrder() throws Exception {
            var coreResponse = getValidObject("files/core/application/query/get.order.info/GetOrderInfoQuery.json", GetOrderInfoResponse.class);
            when(getOrderInfoHandler.on(any())).thenReturn(coreResponse);
            var expectedJson = getJson("files/ui/adapters/http/order/get-order/200_OK.json");


            mockMvc.perform(get("/orders/v1/{orderId}", orderId)
                    .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(expectedJson));
        }

        @Test
        void getOrder_whenCatchNoSuchElementException_thenReturn404() throws Exception {
            when(getOrderInfoHandler.on(any())).thenThrow(new NoSuchElementException());


            mockMvc.perform(get("/orders/v1/{orderId}", orderId)
                    .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound());
        }

    }

    @Nested
    class AssignTest {

        @Test
        void assign() throws Exception {
            mockMvc.perform(post("/orders/v1/{orderId}/assign", orderId)
                    .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isCreated());


            verify(assignOrderHandler).on(any());
        }

        @Test
        void assign_whenCatchNoSuchElementException_thenReturn404() throws Exception {
            when(assignOrderHandler.on(any())).thenThrow(new NoSuchElementException());


            mockMvc.perform(post("/orders/v1/{orderId}/assign", orderId)
                    .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound());
        }

        @Test
        void assign_whenCatchIllegalArgumentException_thenReturn400() throws Exception {
            when(assignOrderHandler.on(any())).thenThrow(new IllegalArgumentException(format("Courier should be in status %s.", CourierStatus.READY)));
            var expectedJson = getJson("files/ui/adapters/http/order/assign/400_BAD_REQUEST_Illegal_status.json");


            mockMvc.perform(post("/orders/v1/{orderId}/assign", orderId)
                    .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().json(expectedJson));
        }

        @Test
        void assign_whenCatchIllegalStateException_thenReturn400() throws Exception {
            when(assignOrderHandler.on(any())).thenThrow(new IllegalStateException(format("No couriers in status %s.", CourierStatus.READY)));
            var expectedJson = getJson("files/ui/adapters/http/order/assign/400_BAD_REQUEST_No_couriers.json");


            mockMvc.perform(post("/orders/v1/{orderId}/assign", orderId)
                    .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().json(expectedJson));
        }

    }
}
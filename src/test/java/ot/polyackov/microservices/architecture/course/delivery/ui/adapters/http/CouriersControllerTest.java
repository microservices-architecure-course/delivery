package ot.polyackov.microservices.architecture.course.delivery.ui.adapters.http;

import static java.lang.String.format;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static ot.polyackov.microservices.architecture.course.delivery.core.domain.util.JsonParseUtil.getJson;

import java.util.NoSuchElementException;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import ot.polyackov.microservices.architecture.course.delivery.core.application.command.finish.working.day.FinishWorkingDayHandler;
import ot.polyackov.microservices.architecture.course.delivery.core.application.command.move.to.location.MoveToLocationHandler;
import ot.polyackov.microservices.architecture.course.delivery.core.application.command.start.working.day.StartWorkingDayHandler;
import ot.polyackov.microservices.architecture.course.delivery.core.domain.courier.aggregate.CourierStatus;

@WebMvcTest(value = {
    CouriersController.class,
})
@AutoConfigureMockMvc(addFilters = false)
class CouriersControllerTest {

    @Autowired
    private MockMvc mockMvc;


    @MockBean
    private StartWorkingDayHandler startWorkingDayHandler;
    @MockBean
    private FinishWorkingDayHandler finishWorkingDayHandler;
    @MockBean
    private MoveToLocationHandler moveToLocationHandler;


    private final String courierId = "cccccccc-cccc-cccc-cccc-000000000000";


    @Nested
    class StartWorkingDayTest {

        @Test
        void startWorkingDa() throws Exception {
            mockMvc.perform(post("/couriers/v1/{courierId}/start-working-day", courierId)
                    .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());


            verify(startWorkingDayHandler).on(any());
        }

        @Test
        void startWorkingDay_whenCatchNoSuchElementException_thenReturn404() throws Exception {
            when(startWorkingDayHandler.on(any())).thenThrow(new NoSuchElementException());


            mockMvc.perform(post("/couriers/v1/{courierId}/start-working-day", courierId)
                    .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound());
        }

        @Test
        void startWorkingDay_whenCatchIllegalArgumentException_thenReturn400() throws Exception {
            when(startWorkingDayHandler.on(any())).thenThrow(new IllegalArgumentException(format("Courier should be in status %s.", CourierStatus.NOT_AVAILABLE)));
            var expectedJson = getJson("files/ui/adapters/http/courier/start-working-day/400_BAD_REQUEST.json");


            mockMvc.perform(post("/couriers/v1/{courierId}/start-working-day", courierId)
                    .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().json(expectedJson));
        }

    }

    @Nested
    class FinishWorkingDayTest {

        @Test
        void finishWorkingDay() throws Exception {
            mockMvc.perform(post("/couriers/v1/{courierId}/finish-working-day", courierId)
                    .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());


            verify(finishWorkingDayHandler).on(any());
        }

        @Test
        void finishWorkingDay_whenCatchNoSuchElementException_thenReturn404() throws Exception {
            when(finishWorkingDayHandler.on(any())).thenThrow(new NoSuchElementException());


            mockMvc.perform(post("/couriers/v1/{courierId}/finish-working-day", courierId)
                    .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound());
        }

        @Test
        void finishWorkingDay_whenCatchIllegalArgumentException_thenReturn400() throws Exception {
            when(finishWorkingDayHandler.on(any())).thenThrow(new IllegalArgumentException(format("Courier should be in status %s.", CourierStatus.NOT_AVAILABLE)));
            var expectedJson = getJson("files/ui/adapters/http/courier/finish-working-day/400_BAD_REQUEST.json");


            mockMvc.perform(post("/couriers/v1/{courierId}/finish-working-day", courierId)
                    .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().json(expectedJson));
        }

    }

    @Nested
    class MoveToLocationTest {

        @Test
        void moveToLocation_whenMoveSuccess_thenReturn() throws Exception {
            mockMvc.perform(post("/couriers/v1/{courierId}/move-to-location", courierId)
                    .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());


            verify(moveToLocationHandler).on(any());
        }

        @Test
        void moveToLocation_whenCatchNoSuchElementException_thenReturn404() throws Exception {
            when(moveToLocationHandler.on(any())).thenThrow(new NoSuchElementException());


            mockMvc.perform(post("/couriers/v1/{courierId}/move-to-location", courierId)
                    .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound());
        }

        @Test
        void moveToLocation_whenCatchIllegalArgumentException_thenReturn400() throws Exception {
            when(moveToLocationHandler.on(any())).thenThrow(new IllegalArgumentException(format("Courier should be in status %s.", CourierStatus.BUSY)));
            var expectedJson = getJson("files/ui/adapters/http/courier/move-to-location/400_BAD_REQUEST.json");


            mockMvc.perform(post("/couriers/v1/{courierId}/move-to-location", courierId)
                    .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().json(expectedJson));
        }
    }
}
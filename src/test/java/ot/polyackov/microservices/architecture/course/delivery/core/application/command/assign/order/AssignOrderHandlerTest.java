package ot.polyackov.microservices.architecture.course.delivery.core.application.command.assign.order;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static ot.polyackov.microservices.architecture.course.delivery.core.domain.util.JsonParseUtil.getObjectList;
import static ot.polyackov.microservices.architecture.course.delivery.core.domain.util.JsonParseUtil.getValidObject;

import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import ot.polyackov.microservices.architecture.course.delivery.core.domain.courier.aggregate.Courier;
import ot.polyackov.microservices.architecture.course.delivery.core.domain.order.aggregate.Order;
import ot.polyackov.microservices.architecture.course.delivery.core.domain_services.DispatchService;
import ot.polyackov.microservices.architecture.course.delivery.core.ports.courier.CourierRepository;
import ot.polyackov.microservices.architecture.course.delivery.core.ports.order.OrderRepository;

@SpringBootTest(classes = {
    AssignOrderHandler.class,

    DispatchService.class,
})
class AssignOrderHandlerTest {

    @Autowired
    private AssignOrderHandler handler;


    @MockBean
    private OrderRepository orderRepository;
    @MockBean
    private CourierRepository courierRepository;


    @Test
    void on_whenOneCourierInReadyStatus_thenReturn() {
        var order = getValidObject("files/core/domain/order.aggregate/Order_CREATED.json", Order.class);
        when(orderRepository.getById(any())).thenReturn(order);
        var courier = getValidObject("files/core/domain/courier.aggregate/Courier_READY.json", Courier.class);
        when(courierRepository.getAllReady()).thenReturn(List.of(courier));


        var actual = handler.on(new AssignOrderCommand(order.getId()));


        assertTrue(actual);
        verify(courierRepository).update(any());
        verify(orderRepository).update(any());
    }

    @Test
    void on_whenMoreThanOneCourierInReadyStatus_thenReturn() {
        var order = getValidObject("files/core/domain/order.aggregate/Order_CREATED.json", Order.class);
        when(orderRepository.getById(any())).thenReturn(order);
        var couriers = getObjectList("files/core/domain/courier.aggregate/Couriers_READY.json", Courier.class);
        when(courierRepository.getAllReady()).thenReturn(couriers);


        var actual = handler.on(new AssignOrderCommand(order.getId()));


        assertTrue(actual);

        verify(courierRepository).update(any());
        verify(orderRepository).update(any());
    }


}
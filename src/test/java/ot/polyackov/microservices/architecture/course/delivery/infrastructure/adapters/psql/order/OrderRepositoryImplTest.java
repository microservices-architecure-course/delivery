package ot.polyackov.microservices.architecture.course.delivery.infrastructure.adapters.psql.order;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static ot.polyackov.microservices.architecture.course.delivery.core.domain.util.JsonParseUtil.getValidObject;

import java.util.List;
import java.util.UUID;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import ot.polyackov.microservices.architecture.course.delivery.core.domain.order.aggregate.Order;
import ot.polyackov.microservices.architecture.course.delivery.core.ports.order.OrderRepository;
import ot.polyackov.microservices.architecture.course.delivery.infrastructure.adapters.psql.PostgresTestConfig;

@SpringBootTest(classes = {
    PostgresTestConfig.class
})
class OrderRepositoryImplTest {

    @Autowired
    private OrderRepository repository;


    @Test
    @Sql("/files/infrastructure/adapters/psql/order/sql/00_truncate_table_orders.sql")
    void add() {
        var order = getValidObject("files/core/domain/order.aggregate/Order_CREATED.json", Order.class);


        var actual = repository.add(order);


        assertEquals(order, actual);
    }


    @Test
    @Sql({
        "/files/infrastructure/adapters/psql/order/sql/00_truncate_table_orders.sql",
        "/files/infrastructure/adapters/psql/order/sql/01_insert_order_CREATED.sql"
    })
    void update() {
        var order = getValidObject("files/core/domain/order.aggregate/Order_ASSIGNED.json", Order.class);


        assertDoesNotThrow(() -> repository.update(order));
    }


    @Test
    @Sql({
        "/files/infrastructure/adapters/psql/order/sql/00_truncate_table_orders.sql",
        "/files/infrastructure/adapters/psql/order/sql/01_insert_order_CREATED.sql"
    })
    void getById() {
        var actual = repository.getById(UUID.fromString("00000000-0000-0000-0000-000000000000"));


        var expected = getValidObject("files/core/domain/order.aggregate/Order_CREATED.json", Order.class);
        assertEquals(expected, actual);
    }


    @Test
    @Sql({
        "/files/infrastructure/adapters/psql/order/sql/00_truncate_table_orders.sql",
        "/files/infrastructure/adapters/psql/order/sql/01_insert_order_CREATED.sql"
    })
    void getAllCreated() {
        var actual = repository.getAllCreated();


        var expected = getValidObject("files/core/domain/order.aggregate/Order_CREATED.json", Order.class);
        assertEquals(List.of(expected), actual);
    }


    @Test
    @Sql({
        "/files/infrastructure/adapters/psql/order/sql/00_truncate_table_orders.sql",
        "/files/infrastructure/adapters/psql/order/sql/02_insert_order_ASSIGNED.sql"
    })
    void getByCourierId() {
        var actual = repository.getByCourierId(UUID.fromString("cccccccc-cccc-cccc-cccc-000000000000"));


        var expected = getValidObject("files/core/domain/order.aggregate/Order_ASSIGNED.json", Order.class);
        assertEquals(expected, actual);
    }

}
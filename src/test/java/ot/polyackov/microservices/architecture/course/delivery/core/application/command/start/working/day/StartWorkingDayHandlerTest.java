package ot.polyackov.microservices.architecture.course.delivery.core.application.command.start.working.day;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static ot.polyackov.microservices.architecture.course.delivery.core.domain.util.JsonParseUtil.getValidObject;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import ot.polyackov.microservices.architecture.course.delivery.core.domain.courier.aggregate.Courier;
import ot.polyackov.microservices.architecture.course.delivery.core.ports.courier.CourierRepository;

@SpringBootTest(classes = {
    StartWorkingDayHandler.class
})
class StartWorkingDayHandlerTest {

    @Autowired
    private StartWorkingDayHandler handler;


    @MockBean
    private CourierRepository courierRepository;


    @Test
    void on_whenCourierInLegalStatus_thenReturn() {
        var courier = getValidObject("files/core/domain/courier.aggregate/Courier_NOT_AVAILABLE.json", Courier.class);
        when(courierRepository.getById(any())).thenReturn(courier);


        var actual = handler.on(new StartWorkingDayCommand(courier.getId()));


        assertTrue(actual);
        verify(courierRepository).update(any());
    }
}
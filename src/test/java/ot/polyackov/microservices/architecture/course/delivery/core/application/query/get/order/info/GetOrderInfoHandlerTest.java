package ot.polyackov.microservices.architecture.course.delivery.core.application.query.get.order.info;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static ot.polyackov.microservices.architecture.course.delivery.core.domain.util.JsonParseUtil.getValidObject;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import ot.polyackov.microservices.architecture.course.delivery.core.domain.order.aggregate.Order;
import ot.polyackov.microservices.architecture.course.delivery.infrastructure.adapters.psql.PostgresTestConfig;

@SpringBootTest(classes = {
    PostgresTestConfig.class
})
class GetOrderInfoHandlerTest {

    @Autowired
    private GetOrderInfoHandler handler;


    @Test
    @Sql({
        "/files/infrastructure/adapters/psql/order/sql/00_truncate_table_orders.sql",
        "/files/infrastructure/adapters/psql/courier/sql/00_truncate_table_couriers.sql",
        "/files/infrastructure/adapters/psql/order/sql/02_insert_order_ASSIGNED.sql",
        "/files/infrastructure/adapters/psql/courier/sql/02_insert_courier_BUSY.sql",
    })
    void on() {
        var order = getValidObject("files/core/domain/order.aggregate/Order_ASSIGNED.json", Order.class);


        var actual = handler.on(new GetOrderInfoQuery(order.getId()));


        var expected = getValidObject("files/core/application/query/get.order.info/GetOrderInfoQuery.json", GetOrderInfoResponse.class);
        assertEquals(expected, actual);
    }
}
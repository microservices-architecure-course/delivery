package ot.polyackov.microservices.architecture.course.delivery.core.domain.util;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import jakarta.validation.Validator;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.stream.Collectors;
import lombok.SneakyThrows;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Assumptions;
import org.springframework.core.io.ClassPathResource;
import ot.polyackov.microservices.architecture.course.delivery.config.ValidatorConfig;


/**
 * Утилита для работы с JSON.
 */
public class JsonParseUtil {

    @SneakyThrows
    public static String getJson(final String pathname) {
        var file = new ClassPathResource(pathname).getFile();
        return FileUtils.readFileToString(file, String.valueOf(StandardCharsets.UTF_8));
    }

    @SneakyThrows
    public static <T> T getNestedObject(final String pathname, final TypeReference<T> typeReference) {
        var json = getJson(pathname);
        return objectMapper().readValue(json, typeReference);
    }

    @Deprecated
    @SneakyThrows
    public static <T> T getNestedObject(final String pathname, final Class<T> parametrized, final Class<?>... parameterClasses) {
        var javaType = objectMapper().getTypeFactory().constructParametricType(parametrized, parameterClasses);

        var json = getJson(pathname);
        return objectMapper().readValue(json, javaType);
    }

    @SneakyThrows
    public static <T> T getValidNestedObject(final String pathname, final TypeReference<T> typeReference) {
        var object = getNestedObject(pathname, typeReference);
        return getValidObjectOrThrow(pathname, object);
    }

    @SneakyThrows
    public static <T> T getObject(final String pathname, final Class<T> parametrized) {
        var json = getJson(pathname);
        return objectMapper().readValue(json, parametrized);
    }

    @SneakyThrows
    public static <T> List<T> getObjectList(final String pathname, final Class<T> parametrized) {
        var javaType = objectMapper().getTypeFactory().constructParametricType(List.class, parametrized);

        var json = getJson(pathname);
        return objectMapper().readValue(json, javaType);
    }

    /**
     * Парсинг валидного json в объект.
     * Пригодится для явного указания, что полученный объект должен быть валидным с точки зрения наложенных ограничений.
     * Позволит определять наличие ошибок тестовых данных на этапе подготовки теста.
     *
     * @param pathname     путь к json
     * @param parametrized класс
     * @return объект
     * @throws IllegalArgumentException исключение с описанием непрошедших валидацию полей.
     */
    public static <T> T getValidObject(final String pathname, final Class<T> parametrized) {
        var object = getObject(pathname, parametrized);

        return getValidObjectOrThrow(pathname, object);
    }

    private static <T> T getValidObjectOrThrow(String pathname, T object) {
        var violationDesc = getViolations(object);

        var isValid = violationDesc.isEmpty();
        if (isValid) {
            return object;
        }

        Assumptions.assumeTrue(false, getValidationDesc(violationDesc, pathname));
        throw new IllegalArgumentException();
    }


    private static final String ERROR_DESCRIPTION_FORMAT = "%s - '%s'";

    private static <T> List<String> getViolations(T request) {
        var violations = validator().validate(request);
        return violations.stream()
            .map(v -> String.format(ERROR_DESCRIPTION_FORMAT, v.getPropertyPath(), v.getMessage()))
            .collect(Collectors.toList());
    }

    private static String getValidationDesc(final List<String> violationDesc, final String pathname) {
        var errorDesc = violationDesc.stream()
            .collect(Collectors.joining(StringUtils.LF, StringUtils.EMPTY, StringUtils.EMPTY));

        return String.format("Object created based on json (%s) is not valid :\n%s", pathname, errorDesc);
    }


    private static ObjectMapper objectMapper() {
        var objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        objectMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        objectMapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
        objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        objectMapper.configure(JsonGenerator.Feature.WRITE_BIGDECIMAL_AS_PLAIN, true);
        return objectMapper;
    }

    private static Validator validator() {
        return new ValidatorConfig().validator();
    }


}

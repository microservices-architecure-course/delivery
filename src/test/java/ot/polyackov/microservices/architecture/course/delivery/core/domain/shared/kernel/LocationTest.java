package ot.polyackov.microservices.architecture.course.delivery.core.domain.shared.kernel;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import ot.polyackov.microservices.architecture.course.delivery.core.domain.courier.aggregate.Transport;

class LocationTest {

    @Nested
    class CreateLocationTest {

        @Test
        void create_whenCoordinateIsCorrect_thenCreate() {

            var actual = Location.create(1, 2);

            assertAll(
                () -> assertEquals(1, actual.abscissaValue()),
                () -> assertEquals(2, actual.ordinateValue())
            );
        }


        @Test
        void create_whenCoordinateIsNotCorrect_thenThrow() {
            var actual = assertThrows(IllegalArgumentException.class, () -> Location.create(0, 2));


            assertEquals("Expected coordinate should be between '1' and '10'. Actual coordinate is '0'.", actual.getMessage());
        }

    }


    @Nested
    class DistanceTo {

        @Test
        void distanceTo() {
            var currentLocation = Location.create(2, 3);
            var targetLocation = Location.create(8, 9);


            var actual = currentLocation.distanceTo(targetLocation);


            assertEquals(12, actual);
        }

        @Test
        void distanceToByAbscissa_whenCurrentLocationEqualsTargetLocation_thenReturnWithoutMove() {
            var currentLocation = Location.create(8, 3);
            var targetLocation = Location.create(8, 3);


            var actual = currentLocation.distanceToByAbscissa(targetLocation);


            var expected = new MoveWrapper(MovementDirection.WITHOUT_MOVE, 0);
            assertEquals(expected, actual);
        }

        @Nested
        class DistanceToByAbscissaTest {

            @Test
            void distanceToByAbscissa_whenCurrentLocationLessThanTargetLocation_thenReturnLeft() {
                var currentLocation = Location.create(2, 3);
                var targetLocation = Location.create(8, 3);


                var actual = currentLocation.distanceToByAbscissa(targetLocation);


                var expected = new MoveWrapper(MovementDirection.LEFT, 6);
                assertEquals(expected, actual);
            }

            @Test
            void distanceToByAbscissa_whenCurrentLocationMoreThanTargetLocation_thenReturnRight() {
                var currentLocation = Location.create(8, 3);
                var targetLocation = Location.create(2, 3);


                var actual = currentLocation.distanceToByAbscissa(targetLocation);


                var expected = new MoveWrapper(MovementDirection.RIGHT, 6);
                assertEquals(expected, actual);
            }

        }


        @Nested
        class DistanceToByOrdinateTest {

            @Test
            void distanceToByOrdinate_whenCurrentLocationLessThanTargetLocation_thenReturnLeft() {
                var currentLocation = Location.create(3, 3);
                var targetLocation = Location.create(3, 6);


                var actual = currentLocation.distanceToByOrdinate(targetLocation);


                var expected = new MoveWrapper(MovementDirection.LEFT, 3);
                assertEquals(expected, actual);
            }

            @Test
            void distanceToByOrdinate_whenCurrentLocationMoreThanTargetLocation_thenReturnRight() {
                var currentLocation = Location.create(3, 6);
                var targetLocation = Location.create(3, 3);


                var actual = currentLocation.distanceToByOrdinate(targetLocation);


                var expected = new MoveWrapper(MovementDirection.RIGHT, 3);
                assertEquals(expected, actual);
            }
        }
    }

    @Nested
    class MoveToTest {

        private final int strideLength = Transport.CAR.getSpeed();

        @Test
        void moveTo_whenIsEnoughOneMove_thenMoveByAbscissa() {
            var currentLocation = Location.create(2, 3);
            var targetLocation = Location.create(4, 6);


            var actual = currentLocation.moveTo(targetLocation, strideLength);


            assertEquals(Location.create(4, 5), actual);
        }

        @Test
        void moveTo_whenCurrentLocationIsLeft_thenMoveByAbscissa() {
            var currentLocation = Location.create(2, 3);
            var targetLocation = Location.create(8, 6);


            var actual = currentLocation.moveTo(targetLocation, strideLength);


            assertEquals(Location.create(6, 3), actual);
        }

        @Test
        void moveTo_whenCurrentLocationIsRight_thenMoveByAbscissa() {
            var currentLocation = Location.create(8, 3);
            var targetLocation = Location.create(2, 6);


            var actual = currentLocation.moveTo(targetLocation, strideLength);


            assertEquals(Location.create(4, 3), actual);
        }

        @Test
        void moveTo_whenIsEnoughOneMove_thenMoveByOrdinate() {
            var currentLocation = Location.create(3, 3);
            var targetLocation = Location.create(3, 6);


            var actual = currentLocation.moveTo(targetLocation, strideLength);


            assertEquals(targetLocation, actual);
        }

        @Test
        void moveTo_whenCurrentLocationIsLeft_thenMoveByOrdinate() {
            var currentLocation = Location.create(3, 2);
            var targetLocation = Location.create(3, 8);


            var actual = currentLocation.moveTo(targetLocation, strideLength);


            assertEquals(Location.create(3, 6), actual);
        }

        @Test
        void moveTo_whenCurrentLocationIsRight_thenMoveByOrdinate() {
            var currentLocation = Location.create(3, 8);
            var targetLocation = Location.create(3, 2);


            var actual = currentLocation.moveTo(targetLocation, strideLength);


            assertEquals(Location.create(3, 4), actual);
        }
    }
}
package ot.polyackov.microservices.architecture.course.delivery.core.application.command.move.to.location;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static ot.polyackov.microservices.architecture.course.delivery.core.domain.util.JsonParseUtil.getValidObject;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import ot.polyackov.microservices.architecture.course.delivery.core.domain.courier.aggregate.Courier;
import ot.polyackov.microservices.architecture.course.delivery.core.domain.order.aggregate.Order;
import ot.polyackov.microservices.architecture.course.delivery.core.ports.courier.CourierRepository;
import ot.polyackov.microservices.architecture.course.delivery.core.ports.order.OrderRepository;

@SpringBootTest(classes = {
    MoveToLocationHandler.class
})
class MoveToLocationHandlerTest {

    @Autowired
    private MoveToLocationHandler handler;


    @MockBean
    private CourierRepository courierRepository;
    @MockBean
    private OrderRepository orderRepository;


    @Test
    void on_whenCourierInLegalStatus_thenReturnFalse() {
        var courier = getValidObject("files/core/domain/courier.aggregate/Courier_BUSY.json", Courier.class);
        when(courierRepository.getById(any())).thenReturn(courier);
        var order = getValidObject("files/core/domain/order.aggregate/Order_ASSIGNED.json", Order.class);
        when(orderRepository.getByCourierId(any())).thenReturn(order);


        var actual = handler.on(new MoveToLocationCommand(courier.getId()));


        assertTrue(actual);
        verify(courierRepository).update(any());
        verify(orderRepository).update(any());
    }

    @Test
    void on_whenCourierNeedMoreThanOneMove_thenReturn() {
        var courier = getValidObject("files/core/domain/courier.aggregate/Courier_isFareFromDeliveryLocation_BUSY.json", Courier.class);
        when(courierRepository.getById(any())).thenReturn(courier);
        var order = getValidObject("files/core/domain/order.aggregate/Order_ASSIGNED.json", Order.class);
        when(orderRepository.getByCourierId(any())).thenReturn(order);


        var actual = handler.on(new MoveToLocationCommand(courier.getId()));


        assertTrue(actual);
        verify(courierRepository).update(any());
        verify(orderRepository, never()).update(any());
    }
}
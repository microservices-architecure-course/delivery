package ot.polyackov.microservices.architecture.course.delivery.core.domain_services;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import ot.polyackov.microservices.architecture.course.delivery.core.domain.courier.aggregate.Courier;
import ot.polyackov.microservices.architecture.course.delivery.core.domain.order.aggregate.Order;
import ot.polyackov.microservices.architecture.course.delivery.core.domain.util.JsonParseUtil;


class DispatchServiceTest {

    private final DispatchService service = new DispatchService();


    @Test
    void getNearestCourier() {
        var order = JsonParseUtil.getValidObject("files/core/domain/order.aggregate/Order_CREATED.json", Order.class);
        var couriers = JsonParseUtil.getObjectList("files/core/domain/courier.aggregate/Couriers_READY.json", Courier.class);


        var actual = service.getNearestCourier(order, couriers);


        var expected = JsonParseUtil.getValidObject("files/core/domain/courier.aggregate/Courier_READY.json", Courier.class);
        Assertions.assertEquals(expected, actual);
    }
}
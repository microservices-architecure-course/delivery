package ot.polyackov.microservices.architecture.course.delivery.infrastructure.adapters.psql.courier;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static ot.polyackov.microservices.architecture.course.delivery.core.domain.util.JsonParseUtil.getValidObject;

import java.util.List;
import java.util.UUID;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import ot.polyackov.microservices.architecture.course.delivery.core.domain.courier.aggregate.Courier;
import ot.polyackov.microservices.architecture.course.delivery.core.ports.courier.CourierRepository;
import ot.polyackov.microservices.architecture.course.delivery.infrastructure.adapters.psql.PostgresTestConfig;


@SpringBootTest(classes = {
    PostgresTestConfig.class
})
class CourierRepositoryImplTest {

    @Autowired
    private CourierRepository repository;


    @Test
    @Sql("/files/infrastructure/adapters/psql/courier/sql/00_truncate_table_couriers.sql")
    void add() {
        var courier = getValidObject("files/core/domain/courier.aggregate/Courier_NOT_AVAILABLE.json", Courier.class);


        var actual = repository.add(courier);


        assertEquals(courier, actual);
    }


    @Test
    @Sql({
        "/files/infrastructure/adapters/psql/courier/sql/00_truncate_table_couriers.sql",
        "/files/infrastructure/adapters/psql/courier/sql/01_insert_courier_READY.sql"
    })
    void update() {
        var courier = getValidObject("files/core/domain/courier.aggregate/Courier_BUSY.json", Courier.class);


        assertDoesNotThrow(() -> repository.update(courier));
    }


    @Test
    @Sql({
        "/files/infrastructure/adapters/psql/courier/sql/00_truncate_table_couriers.sql",
        "/files/infrastructure/adapters/psql/courier/sql/01_insert_courier_READY.sql"
    })
    void getById() {
        var actual = repository.getById(UUID.fromString("cccccccc-cccc-cccc-cccc-000000000000"));


        var expected = getValidObject("files/core/domain/courier.aggregate/Courier_READY.json", Courier.class);
        assertEquals(expected, actual);
    }


    @Test
    @Sql({
        "/files/infrastructure/adapters/psql/courier/sql/00_truncate_table_couriers.sql",
        "/files/infrastructure/adapters/psql/courier/sql/01_insert_courier_READY.sql"
    })
    void getAllReady() {
        var actual = repository.getAllReady();


        var expected = getValidObject("files/core/domain/courier.aggregate/Courier_READY.json", Courier.class);
        assertEquals(List.of(expected), actual);
    }
}
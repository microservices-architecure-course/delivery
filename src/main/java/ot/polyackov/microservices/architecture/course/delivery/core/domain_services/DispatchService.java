package ot.polyackov.microservices.architecture.course.delivery.core.domain_services;

import static java.lang.String.format;

import java.util.Comparator;
import java.util.List;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ot.polyackov.microservices.architecture.course.delivery.core.domain.courier.aggregate.Courier;
import ot.polyackov.microservices.architecture.course.delivery.core.domain.courier.aggregate.CourierStatus;
import ot.polyackov.microservices.architecture.course.delivery.core.domain.order.aggregate.Order;

@Service
public class DispatchService {

    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public Courier getNearestCourier(Order order, List<Courier> couriers) {
        record CourierAndMoveCount(Courier courier, int moveCount) {}

        return couriers.stream()
            .filter(courier -> courier.getTransport().getCapacity().value() >= order.getWeight().value())
            .map(courier -> new CourierAndMoveCount(courier, courier.getMoveCountToDeliveryLocation(order)))
            .min(Comparator.comparing(CourierAndMoveCount::moveCount))
            .map(CourierAndMoveCount::courier)
            .orElseThrow(() -> new IllegalStateException(format("No couriers in status %s.", CourierStatus.READY)));
    }

}

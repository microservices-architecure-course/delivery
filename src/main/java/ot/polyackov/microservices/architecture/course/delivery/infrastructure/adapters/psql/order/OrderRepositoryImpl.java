package ot.polyackov.microservices.architecture.course.delivery.infrastructure.adapters.psql.order;


import java.util.List;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ot.polyackov.microservices.architecture.course.delivery.core.domain.order.aggregate.Order;
import ot.polyackov.microservices.architecture.course.delivery.core.domain.order.aggregate.OrderStatus;
import ot.polyackov.microservices.architecture.course.delivery.core.ports.order.OrderRepository;
import ot.polyackov.microservices.architecture.course.delivery.infrastructure.adapters.psql.order.mapper.OrderMapper;
import ot.polyackov.microservices.architecture.course.delivery.infrastructure.adapters.psql.order.repository.OrderJpaRepository;

@Service
@RequiredArgsConstructor
public class OrderRepositoryImpl implements OrderRepository {

    private final OrderJpaRepository repository;
    private final OrderMapper mapper;

    @Override
    @Transactional
    public Order add(Order order) {
        var entity = mapper.toEntity(order);
        return mapper.toAggregate(repository.saveAndFlush(entity));
    }


    @Override
    @Transactional
    public void update(Order order) {
        var reference = repository.getReferenceById(order.getId());
        var entity = mapper.updateEntity(order, reference);
        repository.saveAndFlush(entity);
    }

    @Override
    @Transactional(readOnly = true)
    public Order getById(UUID id) {
        return repository.findById(id)
            .map(mapper::toAggregate)
            .orElse(null);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Order> getAllCreated() {
        return mapper.toAggregates(
            repository.findAllByStatusIs(OrderStatus.CREATED)
        );
    }

    @Override
    public Order getByCourierId(UUID courierId) {
        return repository.findByCourierId(courierId)
            .map(mapper::toAggregate)
            .orElse(null);
    }
}

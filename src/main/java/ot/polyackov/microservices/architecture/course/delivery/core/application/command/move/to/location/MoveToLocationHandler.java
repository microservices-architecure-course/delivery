package ot.polyackov.microservices.architecture.course.delivery.core.application.command.move.to.location;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ot.polyackov.microservices.architecture.course.delivery.core.ports.courier.CourierRepository;
import ot.polyackov.microservices.architecture.course.delivery.core.ports.order.OrderRepository;

@Service
@Slf4j
@RequiredArgsConstructor
public class MoveToLocationHandler {

    private final CourierRepository courierRepository;
    private final OrderRepository orderRepository;


    public boolean on(MoveToLocationCommand command) {
        var courierId = command.courierId();
        log.info("[{}] Finished working day", courierId);

        var courier = courierRepository.getById(courierId);
        var order = orderRepository.getByCourierId(courierId);

        courier.moveToLocation(order);

        var isReadyToComplete = courier.getCurrentLocation().equals(order.getDeliveryLocation());
        if (isReadyToComplete) {
            order.completed();
            orderRepository.update(order);
        }

        courierRepository.update(courier);
        return true;
    }
}

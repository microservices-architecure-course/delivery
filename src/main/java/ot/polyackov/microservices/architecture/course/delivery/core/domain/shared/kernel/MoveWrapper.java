package ot.polyackov.microservices.architecture.course.delivery.core.domain.shared.kernel;

public record MoveWrapper(MovementDirection movementDirection, int movementDelta) {
}

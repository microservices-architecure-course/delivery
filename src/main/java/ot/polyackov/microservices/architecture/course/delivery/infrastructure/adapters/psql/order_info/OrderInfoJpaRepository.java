package ot.polyackov.microservices.architecture.course.delivery.infrastructure.adapters.psql.order_info;

import java.util.Optional;
import java.util.UUID;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import ot.polyackov.microservices.architecture.course.delivery.infrastructure.adapters.psql.order_info.entity.OrderInfoEntity;

public interface OrderInfoJpaRepository extends Repository<OrderInfoEntity, UUID> {


    @Transactional(readOnly = true)
    @Query(nativeQuery = true,
        value = """
                    SELECT o.id               AS id
                         , c.id               AS courier_id
                         , c.current_location AS courier_location
                    FROM orders o 
                        JOIN couriers c ON c.id = o.courier_id 
                    WHERE o.id = :id
            """)
    Optional<OrderInfoEntity> findById(@Param("id") UUID id);

}

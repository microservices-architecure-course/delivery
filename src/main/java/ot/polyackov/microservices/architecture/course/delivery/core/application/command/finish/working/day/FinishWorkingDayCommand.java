package ot.polyackov.microservices.architecture.course.delivery.core.application.command.finish.working.day;

import java.util.Objects;
import java.util.UUID;

public record FinishWorkingDayCommand(UUID courierId) {

    public FinishWorkingDayCommand {
        Objects.requireNonNull(courierId);
    }

}

package ot.polyackov.microservices.architecture.course.delivery.infrastructure.adapters.psql.courier.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Convert;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import ot.polyackov.microservices.architecture.course.delivery.core.domain.courier.aggregate.CourierStatus;
import ot.polyackov.microservices.architecture.course.delivery.core.domain.courier.aggregate.Transport;
import ot.polyackov.microservices.architecture.course.delivery.core.domain.shared.kernel.Location;
import ot.polyackov.microservices.architecture.course.delivery.infrastructure.adapters.psql.common.entity.BaseEntity;
import ot.polyackov.microservices.architecture.course.delivery.infrastructure.adapters.psql.common.entity.converter.LocationConverter;
import ot.polyackov.microservices.architecture.course.delivery.infrastructure.adapters.psql.common.entity.converter.TransportConverter;
import ot.polyackov.microservices.architecture.course.delivery.infrastructure.adapters.psql.courier.entity.converter.CourierStatusConverter;


@Getter
@Setter
@Entity
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "couriers")
public class CourierEntity extends BaseEntity {

    @Id
    private UUID id;

    @Column(name = "name")
    private String name;

    @Column(name = "status_id")
    @Convert(converter = CourierStatusConverter.class)
    private CourierStatus status;

    @Column(name = "transport_id")
    @Convert(converter = TransportConverter.class)
    private Transport transport;

    @Column(name = "current_location")
    @Convert(converter = LocationConverter.class)
    private Location currentLocation;

}

package ot.polyackov.microservices.architecture.course.delivery.core.domain.courier.aggregate;

import java.util.stream.Stream;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum CourierStatus {

    NOT_AVAILABLE(1, "NotAvailable"),
    READY(2, "Ready"),
    BUSY(3, "Busy"),
    ;

    private final int id;
    private final String name;

    public static boolean isNotAvailable(CourierStatus status) {
        return NOT_AVAILABLE.equals(status);
    }

    public static boolean isReady(CourierStatus status) {
        return READY.equals(status);
    }

    public static boolean isBusy(CourierStatus status) {
        return BUSY.equals(status);
    }


    /**
     * Retrieving an enum element by ID.
     */
    public static CourierStatus of(Integer id) {
        return Stream.of(values())
            .filter(t -> t.getId() == id)
            .findAny()
            .orElse(null);
    }
}

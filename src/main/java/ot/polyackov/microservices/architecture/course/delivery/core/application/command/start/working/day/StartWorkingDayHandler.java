package ot.polyackov.microservices.architecture.course.delivery.core.application.command.start.working.day;

import java.util.NoSuchElementException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ot.polyackov.microservices.architecture.course.delivery.core.ports.courier.CourierRepository;

@Service
@Slf4j
@RequiredArgsConstructor
public class StartWorkingDayHandler {

    private final CourierRepository courierRepository;


    public boolean on(StartWorkingDayCommand command) throws NoSuchElementException, IllegalArgumentException {
        var courierId = command.courierId();
        log.info("[{}] Start working day", courierId);

        var courier = courierRepository.getById(courierId);

        courier.startWorkingDay();

        courierRepository.update(courier);
        return true;
    }
}

package ot.polyackov.microservices.architecture.course.delivery.core.ports.courier;

import java.util.List;
import java.util.UUID;
import ot.polyackov.microservices.architecture.course.delivery.core.domain.courier.aggregate.Courier;

public interface CourierRepository {

    Courier add(Courier courier);

    void update(Courier courier);

    Courier getById(UUID id);

    List<Courier> getAllReady();
}

/**
 * {@link ot.polyackov.microservices.architecure.course.delivery.infrastructure.adapters.rabbit.mq infrastructure.adapters.rabbit.mq}
 * directory contains classes which send requests by RabbitMQ.
 */

package ot.polyackov.microservices.architecture.course.delivery.infrastructure.adapters.rabbit.mq;

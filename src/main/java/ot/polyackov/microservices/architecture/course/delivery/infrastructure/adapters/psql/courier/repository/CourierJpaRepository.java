package ot.polyackov.microservices.architecture.course.delivery.infrastructure.adapters.psql.courier.repository;

import java.util.List;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import ot.polyackov.microservices.architecture.course.delivery.core.domain.courier.aggregate.CourierStatus;
import ot.polyackov.microservices.architecture.course.delivery.infrastructure.adapters.psql.courier.entity.CourierEntity;

public interface CourierJpaRepository extends JpaRepository<CourierEntity, UUID> {

    List<CourierEntity> findAllByStatusIs(CourierStatus status);
}

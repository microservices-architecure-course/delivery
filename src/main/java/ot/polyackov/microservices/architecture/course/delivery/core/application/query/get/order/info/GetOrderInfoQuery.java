package ot.polyackov.microservices.architecture.course.delivery.core.application.query.get.order.info;

import java.util.Objects;
import java.util.UUID;

public record GetOrderInfoQuery(UUID orderId) {

    public GetOrderInfoQuery {
        Objects.requireNonNull(orderId);
    }

}

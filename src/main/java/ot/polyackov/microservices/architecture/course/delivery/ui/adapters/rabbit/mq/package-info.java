/**
 * {@link ot.polyackov.microservices.architecure.course.delivery.ui.adapters.rabbit.mq ui.adapters.rabbit.mq} directory contains classes which receive requests by RabbitMQ.
 */

package ot.polyackov.microservices.architecture.course.delivery.ui.adapters.rabbit.mq;

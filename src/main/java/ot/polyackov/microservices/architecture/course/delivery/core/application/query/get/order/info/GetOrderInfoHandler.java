package ot.polyackov.microservices.architecture.course.delivery.core.application.query.get.order.info;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ot.polyackov.microservices.architecture.course.delivery.infrastructure.adapters.psql.order_info.OrderInfoJpaRepository;

@Service
@RequiredArgsConstructor
public class GetOrderInfoHandler {

    private final OrderInfoJpaRepository repository;


    public GetOrderInfoResponse on(GetOrderInfoQuery query) {
        return repository.findById(query.orderId())
            .map(orderInfo -> new GetOrderInfoResponse(orderInfo.getId(), orderInfo.getCourierId(), orderInfo.getCourierLocation()))
            .orElseThrow();
    }
}

package ot.polyackov.microservices.architecture.course.delivery.core.application.command.finish.working.day;

import java.util.NoSuchElementException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ot.polyackov.microservices.architecture.course.delivery.core.ports.courier.CourierRepository;

@Service
@Slf4j
@RequiredArgsConstructor
public class FinishWorkingDayHandler {

    private final CourierRepository courierRepository;


    public boolean on(FinishWorkingDayCommand command) throws NoSuchElementException, IllegalArgumentException {
        var courierId = command.courierId();
        log.info("[{}] Finished working day", courierId);

        var courier = courierRepository.getById(courierId);

        courier.finishWorkingDay();

        courierRepository.update(courier);
        return true;
    }
}

package ot.polyackov.microservices.architecture.course.delivery.infrastructure.adapters.psql.order.repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import ot.polyackov.microservices.architecture.course.delivery.core.domain.order.aggregate.OrderStatus;
import ot.polyackov.microservices.architecture.course.delivery.infrastructure.adapters.psql.order.entity.OrderEntity;

public interface OrderJpaRepository extends JpaRepository<OrderEntity, UUID> {

    List<OrderEntity> findAllByStatusIs(OrderStatus status);

    Optional<OrderEntity> findByCourierId(UUID courierId);
}

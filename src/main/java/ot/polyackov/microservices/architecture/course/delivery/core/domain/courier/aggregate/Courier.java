package ot.polyackov.microservices.architecture.course.delivery.core.domain.courier.aggregate;

import static java.lang.String.format;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import java.util.UUID;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;
import ot.polyackov.microservices.architecture.course.delivery.core.domain.order.aggregate.Order;
import ot.polyackov.microservices.architecture.course.delivery.core.domain.shared.kernel.Location;

@Getter
@ToString
@EqualsAndHashCode(of = "id")
@Builder(access = AccessLevel.PUBLIC)
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class Courier {

    @NotNull
    @Schema(description = "ID")
    private UUID id;

    @NotNull
    @Schema(description = "The snapshot version of the aggregate")
    private Integer version;

    @NotBlank
    @Schema(description = "Courier name")
    private String name;

    @NotNull
    @Schema(description = "Courier status")
    private CourierStatus status;

    @NotNull
    @Schema(description = "Courier transport")
    private Transport transport;

    @Valid
    @NotNull
    @Schema(description = "Current courier location")
    private Location currentLocation;


    public static Courier create(String name, Transport transport) {
        if (StringUtils.isBlank(name)) {
            throw new IllegalArgumentException("Courier.name should be Not Blank.");
        }

        return Courier.builder()
            .id(UUID.randomUUID())
            .name(name)
            .status(CourierStatus.NOT_AVAILABLE)
            .transport(transport)
            .currentLocation(Location.MIN_LOCATION)
            .build();
    }

    public void startWorkingDay() {
        if (!CourierStatus.isNotAvailable(status)) {
            throw new IllegalArgumentException(format("Courier should be in status %s.", CourierStatus.NOT_AVAILABLE));
        }
        status = CourierStatus.READY;
    }

    public void finishWorkingDay() {
        if (!CourierStatus.isReady(status)) {
            throw new IllegalArgumentException(format("Courier should be in status %s.", CourierStatus.READY));
        }
        status = CourierStatus.NOT_AVAILABLE;
    }

    public void assignOrder(Order order) {
        if (!CourierStatus.isReady(status)) {
            throw new IllegalArgumentException(format("Courier should be in status %s.", CourierStatus.READY));
        }
        this.status = CourierStatus.BUSY;
        order.assignCourier(id);
    }

    public int getMoveCountToDeliveryLocation(Order order) {
        var distance = currentLocation.distanceTo(order.getDeliveryLocation());
        return distance / transport.getSpeed();
    }

    public void moveToLocation(Order order) {
        if (!CourierStatus.isBusy(status)) {
            throw new IllegalArgumentException(format("Courier should be in status %s.", CourierStatus.BUSY));
        }
        var isAssignedToAnotherCourier = !id.equals(order.getCourierId());
        if (isAssignedToAnotherCourier) {
            throw new IllegalArgumentException("Order was assigned to another courier.");
        }

        currentLocation = currentLocation.moveTo(order.getDeliveryLocation(), transport.getSpeed());
    }
}

/**
 * {@link ot.polyackov.microservices.architecure.course.delivery.infrastructure.adapters.grpc infrastructure.adapters.grpc} directory contains classes which send requests by gRPC.
 */

package ot.polyackov.microservices.architecture.course.delivery.infrastructure.adapters.grpc;

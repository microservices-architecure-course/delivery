package ot.polyackov.microservices.architecture.course.delivery.ui.adapters.common.mapper;

/**
 * Mapper for converting from Ui DTO to Core DTO, and vice versa.
 *
 * @param <CoreDtoT> Core DTO
 * @param <UiDtoT>   UI DTO
 */
public interface DtoMapper<CoreDtoT, UiDtoT> {

    default CoreDtoT toCore(UiDtoT uiDto) {
        throw new UnsupportedOperationException();
    }

    default UiDtoT toUi(CoreDtoT coreDto) {
        throw new UnsupportedOperationException();
    }

}

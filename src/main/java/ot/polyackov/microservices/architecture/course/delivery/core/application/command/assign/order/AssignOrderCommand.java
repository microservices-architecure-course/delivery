package ot.polyackov.microservices.architecture.course.delivery.core.application.command.assign.order;

import java.util.Objects;
import java.util.UUID;

public record AssignOrderCommand(UUID orderId) {

    public AssignOrderCommand {
        Objects.requireNonNull(orderId);
    }

}

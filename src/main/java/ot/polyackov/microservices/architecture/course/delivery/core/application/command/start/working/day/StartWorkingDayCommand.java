package ot.polyackov.microservices.architecture.course.delivery.core.application.command.start.working.day;

import java.util.Objects;
import java.util.UUID;

public record StartWorkingDayCommand(UUID courierId) {

    public StartWorkingDayCommand {
        Objects.requireNonNull(courierId);
    }

}

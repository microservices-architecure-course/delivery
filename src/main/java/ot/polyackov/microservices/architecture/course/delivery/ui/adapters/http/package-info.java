/**
 * {@link ot.polyackov.microservices.architecure.course.delivery.ui.adapters.http ui.adapters.http} directory contains classes which receive requests by HTTP.
 */

package ot.polyackov.microservices.architecture.course.delivery.ui.adapters.http;

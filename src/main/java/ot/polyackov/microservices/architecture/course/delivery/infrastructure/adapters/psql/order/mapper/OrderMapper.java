package ot.polyackov.microservices.architecture.course.delivery.infrastructure.adapters.psql.order.mapper;

import java.util.List;
import java.util.Optional;
import org.mapstruct.Mapper;
import ot.polyackov.microservices.architecture.course.delivery.core.domain.order.aggregate.Order;
import ot.polyackov.microservices.architecture.course.delivery.infrastructure.adapters.psql.common.mapper.EntityMapper;
import ot.polyackov.microservices.architecture.course.delivery.infrastructure.adapters.psql.order.entity.OrderEntity;


@Mapper(componentModel = "spring")
public interface OrderMapper extends EntityMapper<OrderEntity, Order> {

    @Override
    Order toAggregate(OrderEntity entity);

    List<Order> toAggregates(List<OrderEntity> entities);

    @Override
    OrderEntity toEntity(Order aggregate);

    @Override
    default OrderEntity updateEntity(Order from, OrderEntity to) {
        var fromOptional = Optional.of(from);

        fromOptional.map(Order::getVersion)
            .ifPresent(to::setVersion);

        fromOptional.map(Order::getCourierId)
            .ifPresent(to::setCourierId);

        fromOptional.map(Order::getDeliveryLocation)
            .ifPresent(to::setDeliveryLocation);

        fromOptional.map(Order::getWeight)
            .ifPresent(to::setWeight);

        return to;
    }

}

package ot.polyackov.microservices.architecture.course.delivery.core.domain.order.aggregate;

import java.util.stream.Stream;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum OrderStatus {

    CREATED(1, "Created"),
    ASSIGNED(2, "Assigned"),
    COMPLETED(3, "Completed");

    private final int id;
    private final String name;

    public static boolean isAssigned(OrderStatus status) {
        return ASSIGNED.equals(status);
    }

    /**
     * Retrieving an enum element by ID.
     */
    public static OrderStatus of(Integer id) {
        return Stream.of(values())
            .filter(t -> t.getId() == id)
            .findAny()
            .orElse(null);
    }
}

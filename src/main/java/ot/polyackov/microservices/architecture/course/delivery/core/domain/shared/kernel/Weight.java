package ot.polyackov.microservices.architecture.course.delivery.core.domain.shared.kernel;

import static java.lang.String.format;

public record Weight(int value) {

    public Weight {
        validatePositiveWeight(value);
    }


    public static Weight create(int value) {
        return new Weight(value);
    }


    private static void validatePositiveWeight(int weight) {
        if (weight < 0) {
            throw new IllegalArgumentException(format("Expected weight should be positive. Actual weight is '%d'.", weight));
        }
    }
}

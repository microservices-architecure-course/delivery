package ot.polyackov.microservices.architecture.course.delivery.infrastructure.adapters.psql.courier;

import java.util.List;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ot.polyackov.microservices.architecture.course.delivery.core.domain.courier.aggregate.Courier;
import ot.polyackov.microservices.architecture.course.delivery.core.domain.courier.aggregate.CourierStatus;
import ot.polyackov.microservices.architecture.course.delivery.core.ports.courier.CourierRepository;
import ot.polyackov.microservices.architecture.course.delivery.infrastructure.adapters.psql.courier.mapper.CourierMapper;
import ot.polyackov.microservices.architecture.course.delivery.infrastructure.adapters.psql.courier.repository.CourierJpaRepository;

@Service
@RequiredArgsConstructor
public class CourierRepositoryImpl implements CourierRepository {

    private final CourierJpaRepository repository;
    private final CourierMapper mapper;

    @Override
    @Transactional
    public Courier add(Courier courier) {
        var entity = mapper.toEntity(courier);
        return mapper.toAggregate(repository.saveAndFlush(entity));
    }

    @Override
    @Transactional
    public void update(Courier courier) {
        var reference = repository.getReferenceById(courier.getId());
        var entity = mapper.updateEntity(courier, reference);
        repository.saveAndFlush(entity);
    }

    @Override
    @Transactional(readOnly = true)
    public Courier getById(UUID id) {
        return repository.findById(id)
            .map(mapper::toAggregate)
            .orElseThrow();
    }

    @Override
    @Transactional(readOnly = true)
    public List<Courier> getAllReady() {
        return mapper.toAggregates(
            repository.findAllByStatusIs(CourierStatus.READY)
        );
    }
}

/**
 * {@link ot.polyackov.microservices.architecure.course.delivery.infrastructure.adapters.http infrastructure.adapters.http} directory contains classes which send requests by HTTP.
 */

package ot.polyackov.microservices.architecture.course.delivery.infrastructure.adapters.http;

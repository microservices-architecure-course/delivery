/**
 * {@link ot.polyackov.microservices.architecure.course.delivery.core core} directory contains classes which process requests.
 */

package ot.polyackov.microservices.architecture.course.delivery.core;

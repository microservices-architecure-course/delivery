package ot.polyackov.microservices.architecture.course.delivery.infrastructure.adapters.psql.courier.mapper;

import java.util.List;
import java.util.Optional;
import org.mapstruct.Mapper;
import org.mapstruct.NullValuePropertyMappingStrategy;
import ot.polyackov.microservices.architecture.course.delivery.core.domain.courier.aggregate.Courier;
import ot.polyackov.microservices.architecture.course.delivery.infrastructure.adapters.psql.common.mapper.EntityMapper;
import ot.polyackov.microservices.architecture.course.delivery.infrastructure.adapters.psql.courier.entity.CourierEntity;


@Mapper(componentModel = "spring", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface CourierMapper extends EntityMapper<CourierEntity, Courier> {

    @Override
    Courier toAggregate(CourierEntity entity);

    List<Courier> toAggregates(List<CourierEntity> entities);

    @Override
    CourierEntity toEntity(Courier aggregate);

    @Override
    default CourierEntity updateEntity(Courier from, CourierEntity to) {
        var fromOptional = Optional.of(from);

        fromOptional.map(Courier::getVersion)
            .ifPresent(to::setVersion);

        fromOptional.map(Courier::getName)
            .ifPresent(to::setName);

        fromOptional.map(Courier::getStatus)
            .ifPresent(to::setStatus);

        fromOptional.map(Courier::getTransport)
            .ifPresent(to::setTransport);

        fromOptional.map(Courier::getCurrentLocation)
            .ifPresent(to::setCurrentLocation);

        return to;
    }

}

package ot.polyackov.microservices.architecture.course.delivery.ui.adapters.http;

import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import ot.polyackov.microservices.architecture.course.delivery.core.application.command.assign.order.AssignOrderCommand;
import ot.polyackov.microservices.architecture.course.delivery.core.application.command.assign.order.AssignOrderHandler;
import ot.polyackov.microservices.architecture.course.delivery.core.application.query.get.order.info.GetOrderInfoHandler;
import ot.polyackov.microservices.architecture.course.delivery.core.application.query.get.order.info.GetOrderInfoQuery;
import ot.polyackov.microservices.architecture.course.delivery.ui.adapters.http.api.OrdersApi;
import ot.polyackov.microservices.architecture.course.delivery.ui.adapters.http.mapper.GetOrderInfoMapper;
import ot.polyackov.microservices.architecture.course.delivery.ui.adapters.http.model.Order;

@Slf4j
@RestController
@RequiredArgsConstructor
public class OrdersController implements OrdersApi {

    private final GetOrderInfoHandler getOrderInfoHandler;
    private final GetOrderInfoMapper getOrderInfoMapper;
    private final AssignOrderHandler assignOrderHandler;

    @Override
    public ResponseEntity<Order> getOrder(UUID orderId) {
        log.info("[{}] Receive GET '/orders/v1/{orderId}'", orderId);
        var handlerResponse = getOrderInfoHandler.on(new GetOrderInfoQuery(orderId));

        var httpResponse = getOrderInfoMapper.toUi(handlerResponse);

        return ResponseEntity.ok()
            .body(httpResponse);
    }

    @Override
    public ResponseEntity<Void> assignOrder(UUID orderId) {
        log.info("[{}] Receive POST '/orders/v1/{orderId}/assign'", orderId);
        assignOrderHandler.on(new AssignOrderCommand(orderId));

        return ResponseEntity
            .status(HttpStatus.CREATED)
            .build();
    }
}

package ot.polyackov.microservices.architecture.course.delivery.core.domain.order.aggregate;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import java.util.UUID;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import ot.polyackov.microservices.architecture.course.delivery.core.domain.shared.kernel.Location;
import ot.polyackov.microservices.architecture.course.delivery.core.domain.shared.kernel.Weight;


@Getter
@ToString
@EqualsAndHashCode(of = "id")
@Builder(access = AccessLevel.PUBLIC)
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class Order {

    @NotNull
    @Schema(description = "ID")
    private UUID id;

    @NotNull
    @Schema(description = "The snapshot version of the aggregate")
    private Integer version;

    @NotNull
    @Schema(description = "Order execution status")
    private OrderStatus status;

    @Schema(description = "Courier ID assigned whose is delivering the order")
    private UUID courierId;

    @Valid
    @NotNull
    @Schema(description = "Location where the order needs to be delivered")
    private Location deliveryLocation;

    @Valid
    @NotNull
    @Schema(description = "Order weight")
    private Weight weight;

    public static Order create(UUID id, Location deliveryLocation, Weight weight) {
        return Order.builder()
            .id(id)
            .status(OrderStatus.CREATED)
            .deliveryLocation(deliveryLocation)
            .weight(weight)
            .build();
    }

    public void assignCourier(UUID courierId) {
        this.status = OrderStatus.ASSIGNED;
        this.courierId = courierId;
    }

    public void completed() {
        if (OrderStatus.isAssigned(status)) {
            this.status = OrderStatus.COMPLETED;
        } else {
            throw new IllegalStateException("Only the assigned order can be " + OrderStatus.COMPLETED);
        }
    }
}

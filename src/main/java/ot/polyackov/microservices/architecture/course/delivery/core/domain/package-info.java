/**
 * {@link ot.polyackov.microservices.architecure.course.delivery.core.domain core.domain} directory contains Aggregate, Entity, and ValueObject classes.
 */

package ot.polyackov.microservices.architecture.course.delivery.core.domain;

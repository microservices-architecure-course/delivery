/**
 * {@link ot.polyackov.microservices.architecure.course.delivery.core core} directory contains interfaces which should be
 * implemented in {@link ot.polyackov.microservices.architecure.course.delivery.infrastructure infrastructure}.
 */

package ot.polyackov.microservices.architecture.course.delivery.core.ports;

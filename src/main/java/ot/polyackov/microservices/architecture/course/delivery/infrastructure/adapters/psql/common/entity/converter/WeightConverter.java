package ot.polyackov.microservices.architecture.course.delivery.infrastructure.adapters.psql.common.entity.converter;

import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;
import ot.polyackov.microservices.architecture.course.delivery.core.domain.shared.kernel.Weight;

/**
 * Converter for {@link Weight}.
 */
@Converter
public class WeightConverter implements AttributeConverter<Weight, Integer> {

    @Override
    public Integer convertToDatabaseColumn(Weight attribute) {
        try {
            return attribute.value();
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public Weight convertToEntityAttribute(Integer dbData) {
        return Weight.create(dbData);
    }
}

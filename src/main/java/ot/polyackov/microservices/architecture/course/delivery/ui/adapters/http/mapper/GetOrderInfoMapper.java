package ot.polyackov.microservices.architecture.course.delivery.ui.adapters.http.mapper;

import org.mapstruct.Mapper;
import ot.polyackov.microservices.architecture.course.delivery.core.application.query.get.order.info.GetOrderInfoResponse;
import ot.polyackov.microservices.architecture.course.delivery.ui.adapters.common.mapper.DtoMapper;
import ot.polyackov.microservices.architecture.course.delivery.ui.adapters.http.model.Order;

@Mapper(componentModel = "spring")
public interface GetOrderInfoMapper extends DtoMapper<GetOrderInfoResponse, Order> {

    @Override
    Order toUi(GetOrderInfoResponse coreDto);
}

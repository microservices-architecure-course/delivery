package ot.polyackov.microservices.architecture.course.delivery.core.ports.order;

import java.util.List;
import java.util.UUID;
import ot.polyackov.microservices.architecture.course.delivery.core.domain.order.aggregate.Order;

public interface OrderRepository {

    Order add(Order order);

    void update(Order order);

    Order getById(UUID id);

    List<Order> getAllCreated();

    Order getByCourierId(UUID courierId);
}

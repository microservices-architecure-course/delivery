package ot.polyackov.microservices.architecture.course.delivery.infrastructure.adapters.psql.order.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Convert;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import ot.polyackov.microservices.architecture.course.delivery.core.domain.order.aggregate.OrderStatus;
import ot.polyackov.microservices.architecture.course.delivery.core.domain.shared.kernel.Location;
import ot.polyackov.microservices.architecture.course.delivery.core.domain.shared.kernel.Weight;
import ot.polyackov.microservices.architecture.course.delivery.infrastructure.adapters.psql.common.entity.BaseEntity;
import ot.polyackov.microservices.architecture.course.delivery.infrastructure.adapters.psql.common.entity.converter.LocationConverter;
import ot.polyackov.microservices.architecture.course.delivery.infrastructure.adapters.psql.common.entity.converter.WeightConverter;
import ot.polyackov.microservices.architecture.course.delivery.infrastructure.adapters.psql.order.entity.converter.OrderStatusConverter;


@Getter
@Setter
@Entity
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "orders")
public class OrderEntity extends BaseEntity {

    @Id
    private UUID id;

    @Column(name = "status_id")
    @Convert(converter = OrderStatusConverter.class)
    private OrderStatus status;

    @Column(name = "courier_id")
    private UUID courierId;

    @Column(name = "delivery_location")
    @Convert(converter = LocationConverter.class)
    private Location deliveryLocation;

    @Column(name = "weight")
    @Convert(converter = WeightConverter.class)
    private Weight weight;

}

package ot.polyackov.microservices.architecture.course.delivery.ui.adapters.http;

import java.util.NoSuchElementException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import ot.polyackov.microservices.architecture.course.delivery.ui.adapters.http.model.Error;

@Slf4j
@ControllerAdvice
public class ControllerErrorHandler {

    @ExceptionHandler(value = {
        IllegalArgumentException.class,
        IllegalStateException.class
    })
    protected ResponseEntity<Object> handleConflict(RuntimeException ex) {
        log.info("Bad request : ", ex);
        var error = new Error(HttpStatus.BAD_REQUEST.value(), ex.getMessage());

        return ResponseEntity.badRequest()
            .body(error);
    }

    @ExceptionHandler(NoSuchElementException.class)
    protected ResponseEntity<?> handleConflict(NoSuchElementException ignore) {
        log.info("Not Found :", ignore);
        return ResponseEntity.notFound()
            .build();
    }
}

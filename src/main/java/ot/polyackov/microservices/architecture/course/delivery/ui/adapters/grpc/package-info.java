/**
 * {@link ot.polyackov.microservices.architecure.course.delivery.ui.adapters.grpc ui.adapters.grpc} directory contains classes which receive requests by gRPC.
 */

package ot.polyackov.microservices.architecture.course.delivery.ui.adapters.grpc;

package ot.polyackov.microservices.architecture.course.delivery.infrastructure.adapters.psql.courier.entity.converter;

import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;
import ot.polyackov.microservices.architecture.course.delivery.core.domain.courier.aggregate.CourierStatus;
import ot.polyackov.microservices.architecture.course.delivery.core.domain.order.aggregate.OrderStatus;

/**
 * Converter for {@link OrderStatus}.
 */
@Converter
public class CourierStatusConverter implements AttributeConverter<CourierStatus, Integer> {

    @Override
    public Integer convertToDatabaseColumn(CourierStatus attribute) {
        try {
            return attribute.getId();
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public CourierStatus convertToEntityAttribute(Integer dbData) {
        return CourierStatus.of(dbData);
    }
}

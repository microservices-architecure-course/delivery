package ot.polyackov.microservices.architecture.course.delivery.core.application.command.move.to.location;

import java.util.Objects;
import java.util.UUID;

public record MoveToLocationCommand(UUID courierId) {

    public MoveToLocationCommand {
        Objects.requireNonNull(courierId);
    }

}

package ot.polyackov.microservices.architecture.course.delivery.infrastructure.adapters.psql.common.entity.converter;

import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;
import ot.polyackov.microservices.architecture.course.delivery.core.domain.courier.aggregate.Transport;
import ot.polyackov.microservices.architecture.course.delivery.core.domain.order.aggregate.OrderStatus;

/**
 * Converter for {@link OrderStatus}.
 */
@Converter
public class TransportConverter implements AttributeConverter<Transport, Integer> {

    @Override
    public Integer convertToDatabaseColumn(Transport attribute) {
        try {
            return attribute.getId();
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public Transport convertToEntityAttribute(Integer dbData) {
        return Transport.of(dbData);
    }
}

package ot.polyackov.microservices.architecture.course.delivery.infrastructure.adapters.psql.common.entity.converter;

import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;
import ot.polyackov.microservices.architecture.course.delivery.core.domain.shared.kernel.Location;

/**
 * Converter for {@link Location}.
 */
@Converter
public class LocationConverter implements AttributeConverter<Location, String> {

    private static final String PATTERN = "%d,%d";
    private static final String COMMA = ",";


    @Override
    public String convertToDatabaseColumn(Location attribute) {
        try {
            return PATTERN.formatted(attribute.abscissaValue(), attribute.ordinateValue());
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public Location convertToEntityAttribute(String dbData) {
        var dbDataArray = dbData.split(COMMA);
        var abscissaValue = Integer.parseInt(dbDataArray[0]);
        var ordinateValue = Integer.parseInt(dbDataArray[1]);
        return Location.create(abscissaValue, ordinateValue);
    }
}

package ot.polyackov.microservices.architecture.course.delivery.config;

import jakarta.validation.Validation;
import jakarta.validation.Validator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Конфигурация для валидации.
 */
@Configuration
public class ValidatorConfig {

    /**
     * Конфигурация валидатора.
     *
     * @return валидатор
     */
    @Bean
    public Validator validator() {
        return Validation.buildDefaultValidatorFactory().getValidator();
    }
}

/**
 * {@link ot.polyackov.microservices.architecure.course.delivery.ui ui} directory contains classes which receive requests.
 */

package ot.polyackov.microservices.architecture.course.delivery.ui;

package ot.polyackov.microservices.architecture.course.delivery.infrastructure.adapters.psql.common.mapper;

/**
 * Mapper for converting from Jpa Entity to DDD.Aggregate, and vice versa.
 *
 * @param <E> Jpa Entity
 * @param <A> Aggregate
 */
public interface EntityMapper<E, A> {

    default A toAggregate(E entity) {
        throw new UnsupportedOperationException();
    }

    default E toEntity(A aggregate) {
        throw new UnsupportedOperationException();
    }

    default E updateEntity(A from, E to) {
        throw new UnsupportedOperationException();
    }
}

package ot.polyackov.microservices.architecture.course.delivery.ui.adapters.http;

import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import ot.polyackov.microservices.architecture.course.delivery.core.application.command.finish.working.day.FinishWorkingDayCommand;
import ot.polyackov.microservices.architecture.course.delivery.core.application.command.finish.working.day.FinishWorkingDayHandler;
import ot.polyackov.microservices.architecture.course.delivery.core.application.command.move.to.location.MoveToLocationCommand;
import ot.polyackov.microservices.architecture.course.delivery.core.application.command.move.to.location.MoveToLocationHandler;
import ot.polyackov.microservices.architecture.course.delivery.core.application.command.start.working.day.StartWorkingDayCommand;
import ot.polyackov.microservices.architecture.course.delivery.core.application.command.start.working.day.StartWorkingDayHandler;
import ot.polyackov.microservices.architecture.course.delivery.ui.adapters.http.api.CouriersApi;


@Slf4j
@RestController
@RequiredArgsConstructor
public class CouriersController implements CouriersApi {

    private final StartWorkingDayHandler startWorkingDayHandler;
    private final FinishWorkingDayHandler finishWorkingDayHandler;
    private final MoveToLocationHandler moveToLocationHandler;

    @Override
    public ResponseEntity<Void> startWorkingDay(UUID courierId) {
        log.info("[{}] Receive POST '/couriers/v1/{courierId}/start-working-day'", courierId);
        startWorkingDayHandler.on(new StartWorkingDayCommand(courierId));

        return ResponseEntity.ok()
            .build();
    }

    @Override
    public ResponseEntity<Void> finishWorkingDay(UUID courierId) {
        log.info("[{}] Receive POST '/couriers/v1/{courierId}/finish-working-day'", courierId);
        finishWorkingDayHandler.on(new FinishWorkingDayCommand(courierId));

        return ResponseEntity.ok()
            .build();
    }

    @Override
    public ResponseEntity<Void> moveToLocation(UUID courierId) {
        log.info("[{}] Receive POST '/couriers/v1/{courierId}/move-to-location'", courierId);
        moveToLocationHandler.on(new MoveToLocationCommand(courierId));

        return ResponseEntity.ok()
            .build();
    }
}

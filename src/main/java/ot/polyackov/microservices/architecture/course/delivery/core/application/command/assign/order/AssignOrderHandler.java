package ot.polyackov.microservices.architecture.course.delivery.core.application.command.assign.order;

import java.util.NoSuchElementException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ot.polyackov.microservices.architecture.course.delivery.core.domain_services.DispatchService;
import ot.polyackov.microservices.architecture.course.delivery.core.ports.courier.CourierRepository;
import ot.polyackov.microservices.architecture.course.delivery.core.ports.order.OrderRepository;

@Service
@Slf4j
@RequiredArgsConstructor
public class AssignOrderHandler {

    private final DispatchService dispatchService;
    private final CourierRepository courierRepository;
    private final OrderRepository orderRepository;


    @Transactional
    public boolean on(AssignOrderCommand command) throws NoSuchElementException, IllegalArgumentException, IllegalStateException {
        var orderId = command.orderId();
        log.info("[{}] Assign order", orderId);

        var order = orderRepository.getById(orderId);
        var couriers = courierRepository.getAllReady();

        var courier = dispatchService.getNearestCourier(order, couriers);

        courier.assignOrder(order);

        courierRepository.update(courier);
        orderRepository.update(order);

        return true;
    }

}

package ot.polyackov.microservices.architecture.course.delivery.core.domain.courier.aggregate;

import java.util.stream.Stream;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import ot.polyackov.microservices.architecture.course.delivery.core.domain.shared.kernel.Weight;

@Getter
@RequiredArgsConstructor
public enum Transport {

    PEDESTRIAN(1, "Pedestrian", 1, Weight.create(1)),
    BICYCLE(2, "Bicycle", 2, Weight.create(4)),
    SCOOTER(3, "Scooter", 3, Weight.create(6)),
    CAR(4, "Car", 4, Weight.create(8)),
    ;

    private final int id;
    private final String name;
    private final int speed;
    private final Weight capacity;


    /**
     * Retrieving an enum element by ID.
     */
    public static Transport of(Integer id) {
        return Stream.of(values())
            .filter(t -> t.getId() == id)
            .findAny()
            .orElse(null);
    }
}

/**
 * {@link ot.polyackov.microservices.architecure.course.delivery.infrastructure infrastructure} directory contains classes which send requests.
 */

package ot.polyackov.microservices.architecture.course.delivery.infrastructure;

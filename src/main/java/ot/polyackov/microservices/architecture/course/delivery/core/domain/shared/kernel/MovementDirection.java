package ot.polyackov.microservices.architecture.course.delivery.core.domain.shared.kernel;

public enum MovementDirection {

    LEFT,
    RIGHT,
    WITHOUT_MOVE;

    public static MovementDirection valueOf(int start, int finish) {
        if (isWithoutMove(start, finish)) {
            return WITHOUT_MOVE;
        }
        var isLeft = start < finish;
        if (isLeft) {
            return LEFT;
        } else {
            return RIGHT;
        }
    }

    public static boolean isWithoutMove(int start, int finish) {
        return start == finish;
    }

    public static boolean isWithoutMove(MovementDirection movementDirection) {
        return WITHOUT_MOVE.equals(movementDirection);
    }

    public static boolean isLeft(MovementDirection movementDirection) {
        return LEFT.equals(movementDirection);
    }

    public static boolean isRight(MovementDirection movementDirection) {
        return RIGHT.equals(movementDirection);
    }

}

package ot.polyackov.microservices.architecture.course.delivery.core.application.query.get.order.info;

import java.util.UUID;
import ot.polyackov.microservices.architecture.course.delivery.core.domain.shared.kernel.Location;

public record GetOrderInfoResponse(UUID id, UUID courierId, Location courierLocation) {
}

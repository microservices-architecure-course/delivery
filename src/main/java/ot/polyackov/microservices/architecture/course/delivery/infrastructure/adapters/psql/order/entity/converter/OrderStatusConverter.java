package ot.polyackov.microservices.architecture.course.delivery.infrastructure.adapters.psql.order.entity.converter;

import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;
import ot.polyackov.microservices.architecture.course.delivery.core.domain.order.aggregate.OrderStatus;

/**
 * Converter for {@link OrderStatus}.
 */
@Converter
public class OrderStatusConverter implements AttributeConverter<OrderStatus, Integer> {

    @Override
    public Integer convertToDatabaseColumn(OrderStatus attribute) {
        try {
            return attribute.getId();
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public OrderStatus convertToEntityAttribute(Integer dbData) {
        return OrderStatus.of(dbData);
    }
}

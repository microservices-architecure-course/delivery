package ot.polyackov.microservices.architecture.course.delivery.infrastructure.adapters.psql.order_info.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Convert;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import ot.polyackov.microservices.architecture.course.delivery.core.domain.shared.kernel.Location;
import ot.polyackov.microservices.architecture.course.delivery.infrastructure.adapters.psql.common.entity.converter.LocationConverter;


@Getter
@Setter
@Entity
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "orders")
public class OrderInfoEntity {

    @Id
    private UUID id;

    @Column(name = "courier_id")
    private UUID courierId;

    @Column(name = "courier_location")
    @Convert(converter = LocationConverter.class)
    private Location courierLocation;
}

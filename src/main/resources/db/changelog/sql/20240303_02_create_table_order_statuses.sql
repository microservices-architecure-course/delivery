--liquibase formatted sql
--changeset polyackov_ot:20240303_02_create_table_order_statuses.sql dbms=postgresql

CREATE TABLE IF NOT EXISTS order_statuses
(
    id          serial primary key,

    name        varchar(64)
);


COMMENT ON COLUMN order_statuses.name IS 'Name of order status';
--rollback COMMENT ON COLUMN order_statuses.name IS NULL;

COMMENT ON TABLE order_statuses IS 'Order statuses table';
--rollback COMMENT ON TABLE order_statuses  IS NULL;


INSERT INTO order_statuses (id, name)
VALUES (1, 'Created'),
       (2, 'Assigned'),
       (3, 'Completed')
ON CONFLICT DO NOTHING;


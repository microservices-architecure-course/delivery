--liquibase formatted sql
--changeset polyackov_ot:20240303_04_create_table_courier_statuses.sql dbms=postgresql

CREATE TABLE IF NOT EXISTS courier_statuses
(
    id          serial primary key,

    name        varchar(64)
);


COMMENT ON COLUMN courier_statuses.name IS 'Name of courier status';
--rollback COMMENT ON COLUMN courier_statuses.name IS NULL;

COMMENT ON TABLE courier_statuses IS 'Courier statuses table';
--rollback COMMENT ON TABLE courier_statuses  IS NULL;


INSERT INTO courier_statuses (id, name)
VALUES (1, 'NotAvailable'),
       (2, 'Ready'),
       (3, 'Busy')
ON CONFLICT DO NOTHING;

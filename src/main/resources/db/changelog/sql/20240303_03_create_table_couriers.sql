--liquibase formatted sql
--changeset polyackov_ot:20240303_03_create_table_couriers.sql dbms=postgresql

CREATE TABLE IF NOT EXISTS couriers
(
    id                     uuid                    not null
    constraint pk_couriers primary key,

    created_at             timestamp default now() not null,
    modified_at            timestamp default now() not null,
    version                integer   default 0     not null,

    name                   varchar(256),
    status_id              smallint,
    transport_id           smallint,
    current_location       varchar(64)

);


COMMENT ON COLUMN couriers.status_id IS 'Courier name';
--rollback COMMENT ON COLUMN couriers.status_id IS NULL;

COMMENT ON COLUMN couriers.status_id IS 'Courier status ID';
--rollback COMMENT ON COLUMN couriers.status_id IS NULL;

COMMENT ON COLUMN couriers.transport_id IS 'Courier transport ID';
--rollback COMMENT ON COLUMN couriers.transport_id IS NULL;

COMMENT ON COLUMN couriers.current_location IS 'Current courier location';
--rollback COMMENT ON COLUMN couriers.current_location IS NULL;

COMMENT ON TABLE couriers IS 'Courier table';
--rollback COMMENT ON TABLE couriers  IS NULL;

INSERT INTO couriers(id, name, transport_id, current_location, status_id)
VALUES ('bf79a004-56d7-4e5f-a21c-0a9e5e08d10d', 'Петя', 1, '1,1', 1),
       ('a9f7e4aa-becc-40ff-b691-f063c5d04015', 'Оля', 1, '1,1', 1),
       ('db18375d-59a7-49d1-bd96-a1738adcee93', 'Ваня', 2, '1,1', 1),
       ('e7c84de4-3261-476a-9481-fb6be211de75', 'Маша', 2, '1,1', 1),
       ('407f68be-5adf-4e72-81bc-b1d8e9574cf8', 'Игорь', 3, '1,1', 1),
       ('006e6c66-087e-4a27-aa59-3c0a2bc945c5', 'Даша', 3, '1,1', 1),
       ('40d50b82-ce79-4cde-8ce1-21883f466038', 'Сережа', 4, '1,1', 1),
       ('18e5ba41-6710-4143-9808-704e88e94bd9', 'Катя', 4, '1,1', 1);
### Goal

Сервис доставки

### Description

### Swagger Doc

- [api-docs](http://localhost:8080/v3/api-docs)
- [swagger-config](http://localhost:8080/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config)

### Actuator Link

- [actuator-main](http://localhost:8080/actuator)
- [actuator-metrics](http://localhost:8080/actuator/metrics)

### How to run locally

#### Requirements

- Java 17
- Spring Boot 3.2.*

#### VM Options

## Sequence Diagram

## Component Diagram

